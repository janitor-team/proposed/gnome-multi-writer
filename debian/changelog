gnome-multi-writer (3.32.1-1) unstable; urgency=medium

  * New upstream release
  * Release to unstable

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 22 Sep 2019 09:26:23 -0400

gnome-multi-writer (3.32.0-1) experimental; urgency=medium

  * New upstream release
  * Bump minimum meson to 0.46.0
  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Build-Depend on dh-sequence-gnome
  * Stop overriding libexecdir

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 08 Mar 2019 10:06:07 -0500

gnome-multi-writer (3.30.0-2) unstable; urgency=medium

  * Add -Wl,-O1 to our LDFLAGS
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 25 Dec 2018 07:54:45 -0500

gnome-multi-writer (3.30.0-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.2.1
  * Drop obsolete intltool Build-Depends

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 04 Sep 2018 10:03:15 -0400

gnome-multi-writer (3.28.0-1) unstable; urgency=medium

  * New upstream release
  * Drop patch: Applied in new release

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 15 Mar 2018 11:07:35 -0400

gnome-multi-writer (3.26.0-2) unstable; urgency=medium

  * Update Vcs fields for migration to https://salsa.debian.org/
  * Add debian/gbp.conf
  * Bump Standards-Version to 4.1.3
  * Bump debhelper compat to 11
  * Use appstream-util appdata-to-news to install NEWS
  * Cherry-pick do-not-hardcode-installation-paths.patch
    to fix libexecdir install

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 26 Feb 2018 22:26:19 -0500

gnome-multi-writer (3.26.0-1) unstable; urgency=medium

  * New upstream translations release

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 11 Sep 2017 16:00:49 -0400

gnome-multi-writer (3.25.90-1) unstable; urgency=medium

  * New upstream release
  * Build with meson
  * debian/control.in:
    - Build-depend on policykit-1
  * Drop 0001-build-Don-t-remove-NEWS-and-ChangeLog-on-distclean.patch:
    obsolete
  * Bump Standards-Version to 4.1.0

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 29 Aug 2017 19:37:51 -0400

gnome-multi-writer (3.22.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop explicit Build-Depends on dh-autoreconf. No longer needed with
    debhelper 10.

 -- Michael Biebl <biebl@debian.org>  Wed, 12 Oct 2016 15:43:12 +0200

gnome-multi-writer (3.22.0-1) unstable; urgency=medium

  * New upstream release.
  * Use non-multiarch path (/usr/lib/gnome-multi-writer) for libexecdir.

 -- Michael Biebl <biebl@debian.org>  Tue, 20 Sep 2016 10:31:30 +0200

gnome-multi-writer (3.21.92-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * debian/docs: Install NEWS too
  * Updated lintian overrides

  [ Michael Biebl ]
  * Mark binary package as linux-any. The build dependencies libgudev and
    libgusb are not optional and only available on Linux.
  * New upstream development release.
  * Drop 0002-build-move-gnome-multi-writer-probe-to-libexecdir.patch, merged
    upstream.

 -- Michael Biebl <biebl@debian.org>  Tue, 13 Sep 2016 15:23:49 +0200

gnome-multi-writer (3.21.91-1) unstable; urgency=medium

  * New upstream release.
  * Stop patching the generated man pages, upstream groff nowadays renders
    both - and \- as HYPHEN-MINUS.
  * Drop hard-coded Depends on libgl1-mesa-glx.
  * Bump debhelper compat to 10
  * Don't manually build-depend on and run autotools-dev, this is automatic
    in compat 10
  * Update Build-Depends as per configure.ac:
    - Add gobject-introspection, appstream-util and replace itstool with
      yelp-tools. Those are required for running autoreconf.
    - Add libgudev-1.0-dev and libpolkit-gobject-1-dev.
    - Build-depend on autoconf-archive instead of gnome-common
      (Closes: #829940)
  * Replace makefile.in_not_rm.patch and path_to_probe.patch with proper
    patches that can be upstreamed.

 -- Michael Biebl <biebl@debian.org>  Sun, 04 Sep 2016 16:22:29 +0200

gnome-multi-writer (3.21.90-4) unstable; urgency=medium

  * Package adopted by the pkg-gnome-maintainers team. (Closes: #836337)

 -- Michael Biebl <biebl@debian.org>  Sun, 04 Sep 2016 14:20:46 +0200

gnome-multi-writer (3.21.90-3) unstable; urgency=medium

  * QA upload
  * Orphaning package.

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Thu, 01 Sep 2016 17:10:39 -0300

gnome-multi-writer (3.21.90-2) unstable; urgency=medium

  * debian/rules:
      - override_dh_auto_configure: --enable-compile-warnings=no.

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Fri, 19 Aug 2016 16:20:29 -0300

gnome-multi-writer (3.21.90-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
      - updated my email
      - bump Standards-Version: from 3.9.7 to 3.9.8.
  * debian/copyright:
      - Source entry updated to github.
      - updated my email.
      - updated two po entries.

 -- Herbert Parentes Fortes Neto <hpfn@debian.org>  Thu, 18 Aug 2016 09:28:49 -0300

gnome-multi-writer (3.20.0-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright updated.

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Tue, 19 Apr 2016 13:51:07 -0300

gnome-multi-writer (3.19.90-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
      - Build-Depends:
          - intltool >= 0.50.0.
          - pkg-config >= 0.16.
      - Bump Standards-Version to 3.9.7.
      - Vcs-git using https.
  * debian/copyright updated.
  * debian/gnome-multi-writer.xpm file removed.
  * debian/patches:
      - makefile.in_not_rm_local.patch updated.
      - README file created to inform about where gnome-multi-writer.patch
        is used.
  * debian/rules:
      - lines to install .xpm file removed.
  * debian/watch:
      - version 4.

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Sun, 21 Feb 2016 10:12:36 -0300

gnome-multi-writer (3.18.0-2) unstable; urgency=low

  * debian/copyright updated.
  * debian/rules:
      - libglib2.0-dev (>= 2.46),
        libgusb-dev (>= 0.2.7),
        (Closes: #803034). Thanks Sebastien Bacher.

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Sat, 23 Jan 2016 11:45:49 -0200

gnome-multi-writer (3.18.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright updated.
  * debian/menu:
      - removed. A .desktop file (upstream) exists:
        https://lists.debian.org/debian-devel-announce/2015/09/msg00000.html

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Wed, 07 Oct 2015 14:33:36 -0300

gnome-multi-writer (3.17.92-1) unstable; urgency=medium

  * New upstream release.
  * debian/clean: created
      - config.log
      - po/POTFILES
  * debian/patches:
      - path_to_probe.patch updated

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Sun, 20 Sep 2015 12:54:41 -0300

gnome-multi-writer (3.17.90-1) unstable; urgency=medium

  * New upstream release
  * debian/copyright:
      - entries added:
          - po/Makefile.in.in
          - po/id.po
          - po/oc.po
      - entries updated:
          - po/ca.po.
          - po/es.po.
          - po/it.po.
          - po/sv.po.
  * debian/patches:
      - makefile.in_not_rm.patch updated
      - gnome-multi-writer.patch headers added

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Sat, 29 Aug 2015 15:25:22 -0300

gnome-multi-writer (3.16.0-1) unstable; urgency=low

  * Initial release (Closes: #786430)

 -- Herbert Parentes Fortes Neto <hpfn@ig.com.br>  Sun, 21 Jun 2015 15:19:13 -0300
